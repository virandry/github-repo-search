const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');
const path = require('path');

const APP_DIR = path.resolve(__dirname, 'src');
const OUTPUT_DIR = path.resolve(__dirname, 'dist');

module.exports = (env, argv) => {
  console.log('env: ', env);
  const inspectBundle = env ? env.inspectBundle : false;
  const isEnvProduction = argv.mode === 'production';
  console.log(isEnvProduction);
  let config = {
    entry: {
      index: `${APP_DIR}/index.js`,
    },
    output: {
      path: OUTPUT_DIR,
      filename: 'static/js/[name].[hash].js',
      publicPath: path.resolve(__dirname, '/'),
      globalObject: 'this',
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          include: APP_DIR,
          // exclude: [/node_modules/],
          use: [
            {
              loader: 'thread-loader',
            },
            {
              loader: 'babel-loader',
            },
          ],
        },
        {
          test: /\.module\.(sa|sc|c)ss$/,
          use: [
            !isEnvProduction ? 'style-loader' : MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                modules: { localIdentName: '[name][local][hash:base64:3]' },
                sourceMap: true,
              },
            },
            /*{
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: [
                  // eslint-disable-next-line global-require
                  require('postcss-import'),
                  // eslint-disable-next-line global-require
                  require('postcss-preset-env')({ preserve: false }),
                  // eslint-disable-next-line global-require
                  require('autoprefixer')({ flexbox: 'no-2009' }),
                  // eslint-disable-next-line global-require
                  require('postcss-custom-media'),
                  // eslint-disable-next-line global-require
                  require('postcss-calc'),
                ],
              },
            },*/
            {
              loader: 'sass-loader',
              options: {
                sourceMap: !isEnvProduction,
              },
            },
          ],
        },
        {
          test: /\.(sa|sc|c)ss$/,
          exclude: [/\.module\.(sa|sc|c)ss$/],
          use: [
            !isEnvProduction ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            /*{
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: [
                  // eslint-disable-next-line global-require
                  require('postcss-import'),
                  // eslint-disable-next-line global-require
                  require('postcss-preset-env')({ preserve: false }),
                  // eslint-disable-next-line global-require
                  require('autoprefixer')({ flexbox: 'no-2009' }),
                  // eslint-disable-next-line global-require
                  require('postcss-custom-media'),
                  // eslint-disable-next-line global-require
                  require('postcss-calc'),
                ],
              },
            },*/
            {
              loader: 'sass-loader',
              options: {
                sourceMap: !isEnvProduction,
              },
            },
          ],
        },
        {
          loader: 'file-loader',
          exclude: [/\.(js|mjs|jsx|ts|tsx)$/, /\.html$/, /\.json$/, /\.(sa|sc|c)ss$/],
          options: {
            name: 'static/media/[name].[ext]',
          },
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin({ dry: true }),
      new HtmlWebpackPlugin({
        hash: true,
        template: './src/public/template.html',
        filename: 'index.html',
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
          removeRedundantAttributes: true,
          collapseInlineTagWhitespace: true,
        },
      }),
      new MiniCssExtractPlugin({
        filename: '[name].[hash].css',
        chunkFilename: '[id].[name].[hash].css',
      }),
      new webpack.EnvironmentPlugin({
        NODE_ENV: isEnvProduction ? 'production' : 'development', // use 'development' unless process.env.NODE_ENV is defined
        DEBUG: !isEnvProduction,
        USERNAME: env ? env.USERNAME : null,
        TOKEN: env ? env.TOKEN : null,
      }),
    ],
    resolve: {
      modules: [APP_DIR, 'node_modules'],
      extensions: ['.js', '.json', '.jsx', '.ts', '.tsx'],
    },
    optimization: {
      minimize: isEnvProduction,
      minimizer: [
        // This is only used in production mode
        new TerserPlugin({
          terserOptions: {
            compress: {
              warnings: false,
            },
            mangle: true,
            output: {
              ecma: 5,
              comments: false,
              ascii_only: true,
            },
          },
        }),
      ],
    },
  };

  if (inspectBundle) {
    config = {
      ...config,
      plugins: [...config.plugins, new BundleAnalyzerPlugin()],
    };
  }

  if (!isEnvProduction) {
    return {
      ...config,
      devServer: {
        contentBase: path.join(__dirname, 'dist'),
        writeToDisk: true,
      },
    };
  }

  return {
    ...config,
    plugins: [
      ...config.plugins,
      new CompressionPlugin(),
      new CompressionPlugin({
        filename: '[path].br[query]',
        algorithm: 'brotliCompress',
      }),
    ],
  };
};
