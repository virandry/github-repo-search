import React from 'react';
import Search from './routes/Search';
import 'styles/global.scss';
import { Switch, Route } from 'react-router-dom';
import Admin from './routes/Admin';
import Login from './routes/Login';
import ProtectedContainer from './routes/Admin/containers/ProtectedContainer';

function App() {
  return (
    <Switch>
      <Route path="/" exact>
        <Search />
      </Route>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/admin">
        <ProtectedContainer>
          <Admin />
        </ProtectedContainer>
      </Route>
    </Switch>
  );
}

export default App;
