import React from 'react';
import { render, cleanup } from '@testing-library/react';
import App from '../App';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import LocalStorageUtil from '../utils/LocalStorageUtil';

afterEach(cleanup);

describe('App Router', () => {
  it('renders Search route', () => {
    const history = createMemoryHistory({ initialEntries: ['/'] });
    const { getByTestId, getByText } = render(
      <Router history={history}>
        <App />
      </Router>,
    );

    expect(getByTestId('search-page')).toBeInTheDocument();
    expect(getByText(/Github Repo Search Engine/i)).toBeInTheDocument();
  });

  it('renders Login route', () => {
    const history = createMemoryHistory({ initialEntries: ['/login'] });
    const { getByTestId, getByText } = render(
      <Router history={history}>
        <App />
      </Router>,
    );

    expect(getByTestId('login-page')).toBeInTheDocument();
    expect(getByText(/Github Repo Search Engine/i)).toBeInTheDocument();
  });

  it('renders Login route when routing to admin without login', () => {
    const history = createMemoryHistory({ initialEntries: ['/admin'] });
    const { getByTestId } = render(
      <Router history={history}>
        <App />
      </Router>,
    );

    expect(getByTestId('login-page')).toBeInTheDocument();
  });

  it('renders Admin route when when user is logged in', () => {
    // assume user is logged in by having token in the localstorage
    LocalStorageUtil.setItem('encodedBasic', btoa('DUMMY:DUMMY'));
    const history = createMemoryHistory({ initialEntries: ['/admin'] });
    const { getByTestId } = render(
      <Router history={history}>
        <App />
      </Router>,
    );

    expect(getByTestId('admin-page')).toBeInTheDocument();
  });
});
