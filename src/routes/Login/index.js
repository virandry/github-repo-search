import React, { useCallback, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import styles from './Login.module.scss';
import { Logo } from '../../components/Logo';
import { Col, Row } from 'react-flexbox-grid';
import Input from '../../components/Input';
import Button from '../../components/Button/Button';
import useFormInput from '../../hooks/useFormInput';
import LocalStorageUtil from '../../utils/LocalStorageUtil';
import AuthService from '../../services/AuthService';

function Login() {
  const history = useHistory();
  const username = useFormInput('');
  const password = useFormInput('');

  useEffect(() => {
    if (LocalStorageUtil.getItem('encodedBasic')) LocalStorageUtil.removeItem('encodedBasic');
  }, []);

  const handleSubmit = useCallback(
    async event => {
      event.preventDefault();
      try {
        const token = await AuthService.login(username.value, password.value);
        const encodedBasic = btoa(`${username.value}:${token}`);
        LocalStorageUtil.setItem('encodedBasic', encodedBasic);
        history.push('/admin');
      } catch (e) {
        alert('Login Error');
      }
    },
    [username.value, password.value, history],
  );

  return (
    <div data-testid="login-page" className={styles.login}>
      <div className={styles.container}>
        <Logo admin />
        <form onSubmit={handleSubmit} className={styles.form}>
          <Row>
            <Col xs={12} sm={6} className={styles.col}>
              <Input label="Username" id="username" name="username" {...username} required />
            </Col>
            <Col xs={12} sm={6} className={styles.col}>
              <Input type="password" label="Password" id="password" name="password" {...password} required />
            </Col>
            <Col xs={12} sm={12} className={styles.buttonCol}>
              <Button data-testid="login-btn" kind="brand" type="submit">
                Login
              </Button>
            </Col>
          </Row>
          <div className={styles.decoration} />
        </form>
      </div>
      <Link className="navLink" to="/">
        Search Page
      </Link>
    </div>
  );
}

export default Login;
