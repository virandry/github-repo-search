import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import App from '../../../App';
import { GITHUB_RESULT, GITHUB_RESULT_PAGE_2, LANGUAGE_INPUT, TOPIC_INPUT } from './fixtures';
import GithubService from '../../../services/GithubService';
import AuditService from '../../../services/AuditService';

jest.mock('../../../services/GithubService');
jest.mock('../../../services/AuditService');

describe('Search Page', () => {
  const history = createMemoryHistory({ initialEntries: ['/'] });

  beforeAll(() => {
    GithubService.searchRepo.mockResolvedValue(GITHUB_RESULT);
    AuditService.log.mockResolvedValue();
  });

  it('performs search', async () => {
    const { getByLabelText, getByTestId, findByTestId, findByText } = render(
      <Router history={history}>
        <App />
      </Router>,
    );

    const topicInput = getByLabelText(/topic/i);
    const languageInput = getByLabelText(/language/i);

    topicInput.value = TOPIC_INPUT;
    languageInput.value = LANGUAGE_INPUT;

    const button = getByTestId(/search-btn/i);

    fireEvent.click(button);

    // assertions
    expect(GithubService.searchRepo).toHaveBeenCalledTimes(1);

    const itemsContainer = await findByTestId('search-result');
    const pageInfo = await findByText(/Page 1 of 2/i);
    expect(pageInfo).toBeInTheDocument();
    expect(itemsContainer).toHaveTextContent(TOPIC_INPUT);
    expect(itemsContainer).toHaveTextContent(LANGUAGE_INPUT);
    expect(itemsContainer).toHaveTextContent(GITHUB_RESULT.items[0].name);
  });

  it('can navigate to other page', async () => {
    const { getByLabelText, getByTestId, findByTestId, findByText } = render(
      <Router history={history}>
        <App />
      </Router>,
    );

    const topicInput = getByLabelText(/topic/i);
    const languageInput = getByLabelText(/language/i);

    topicInput.value = TOPIC_INPUT;
    languageInput.value = LANGUAGE_INPUT;

    const button = getByTestId(/search-btn/i);

    fireEvent.click(button);

    const pagination = await findByTestId('pagination');

    GithubService.searchRepo.mockResolvedValue(GITHUB_RESULT_PAGE_2);
    expect(pagination).toBeInTheDocument();
    const buttonPage2 = await findByText('2', { exact: true });
    fireEvent.click(buttonPage2);
    const pageInfo = await findByText(/Page 2 of 2/i);
    expect(pageInfo).toBeInTheDocument();
    const itemsContainer = await findByTestId('search-result');
    expect(itemsContainer).toHaveTextContent(TOPIC_INPUT);
    expect(itemsContainer).toHaveTextContent(LANGUAGE_INPUT);
    expect(itemsContainer).toHaveTextContent(GITHUB_RESULT_PAGE_2.items[0].name);
  });
});
