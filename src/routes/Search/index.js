import React, { useState, useCallback } from 'react';
import useFormInput from '../../hooks/useFormInput';
import GithubService from '../../services/GithubService';
import AuditService from '../../services/AuditService';
import Pagination from '@material-ui/lab/Pagination';
import { Col, Row } from 'react-flexbox-grid';
import Input from '../../components/Input';
import styles from './Search.module.scss';
import { Link } from 'react-router-dom';
import { SearchAction } from '../../constants/SearchAction';
import Button from '../../components/Button/Button';
import { Card, CardBasicInfo, CardMeta, CardThumbnail, CardContent } from '../../components/Card';
import Label from '../../components/Label';
import ItemsLoading from './components/ItemsLoading';
import { Logo } from '../../components/Logo';

const queryInit = {
  topic: '',
  language: '',
  page: 1,
  perPage: 12,
};
const itemsInit = [];

function Search() {
  const topic = useFormInput('');
  const language = useFormInput('');
  const [items, setItems] = useState(itemsInit);
  const [currentPage, setCurrentPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [loading, setLoading] = useState(false);

  const logAudit = useCallback(async (query, response, action) => {
    const audit = {
      topic: query.topic, // search based on topic
      language: query.language, // search based on topic + language
      url: response.url,
      result: response.items, // dump github response
      page: response.currentPage,
      perPage: query.perPage,
      totalPage: response.totalPage,
      totalCount: response.totalCount,
      action,
    };

    console.log(audit);
    AuditService.log(audit);
  }, []);

  const fetchRepo = useCallback(async query => {
    try {
      const response = await GithubService.searchRepo(query);
      console.log('githubResponse: ', response);
      setItems(response.items);
      setTotalPage(response.totalPage);
      setCurrentPage(response.currentPage);

      return response;
    } catch (e) {
      console.log(e?.data?.message);
    }
  }, []);

  const handlePageChange = useCallback(
    async (event, value) => {
      setLoading(true);
      document.getElementById('results').scrollIntoView({ behavior: 'smooth' });
      try {
        const query = { ...queryInit, topic: topic.value, language: language.value, page: value };
        const response = await fetchRepo(query);
        logAudit(query, response, SearchAction.PAGE_CHANGE);
      } catch (e) {
        console.log(e?.data?.message);
      } finally {
        setLoading(false);
      }
    },
    [logAudit, fetchRepo, topic.value, language.value],
  );

  const handleSubmit = useCallback(
    async event => {
      event.preventDefault();
      if (loading) return;
      if (!(topic.value || language.value)) {
        alert('Please input at least one of the input fields.');
        return;
      }
      setLoading(true);
      try {
        const query = { ...queryInit, topic: topic.value, language: language.value };
        const response = await fetchRepo(query);
        logAudit(query, response, SearchAction.SUBMIT);
      } catch (e) {
        console.log(e?.data?.message);
      } finally {
        setLoading(false);
      }
    },
    [loading, logAudit, fetchRepo, topic.value, language.value],
  );

  return (
    <div data-testid="search-page" className={styles.search}>
      <div className={styles.container}>
        <Logo />
        <form onSubmit={handleSubmit} className={styles.form}>
          <Row>
            <Col xs={12} sm={6} className={styles.col}>
              <Input label="Topic" id="topic" name="topic" {...topic} />
            </Col>
            <Col xs={12} sm={6} className={styles.col}>
              <Input label="Language" id="language" name="language" {...language} />
            </Col>
            <Col xs={12} sm={12} className={styles.buttonCol}>
              <Button data-testid="search-btn" kind="brand" type="submit">
                Search
              </Button>
            </Col>
          </Row>
          <div className={styles.decoration} />
        </form>
        <Row data-testid="search-result" id="results" className={styles.items}>
          {!!currentPage && !!totalPage && (
            <Col xs={12} className={styles.pageInfo}>
              <span>{`Page ${currentPage} of ${totalPage}`}</span>
            </Col>
          )}
          {items.map(item => {
            return (
              <Col key={item.id} xs={12} sm={6} md={3} lg={2}>
                <Card id={item.id}>
                  <CardThumbnail src={item.owner?.avatar_url} alt={item.name}>
                    <CardBasicInfo title={item.name} caption={item.language} />
                  </CardThumbnail>
                  <CardContent>
                    <CardMeta stars={item.stargazers_count} forks={item.forks} url={item.html_url} />
                  </CardContent>
                  <CardContent>
                    <div>
                      {item.topics.map((tTopic, index) => (
                        <Label key={index} highlight={tTopic?.toLowerCase() === topic.value?.toLowerCase()}>
                          {tTopic}
                        </Label>
                      ))}
                    </div>
                  </CardContent>
                </Card>
              </Col>
            );
          })}
          {loading && <ItemsLoading />}
        </Row>
        <Row>
          <Col data-testid="pagination" xs={12} className={styles.paginationContainer}>
            {!!totalPage && <Pagination size="large" count={totalPage} page={currentPage} onChange={handlePageChange} />}
          </Col>
        </Row>
      </div>
      <Link className="navLink" to="/admin">
        ADMIN
      </Link>
    </div>
  );
}

export default Search;
