import React, { useCallback, useEffect, useState } from 'react';
import ReportService from '../../../services/ReportService';
import Statistic from '../../../components/Stats/Statistic';

function NumberOfSearches() {
  const [numOfSearches, setNumOfSearches] = useState(0);

  const getTotalNumberOfSearch = useCallback(async () => {
    const response = await ReportService.getTotalNumberOfSearch();
    setNumOfSearches(response);
  }, []);

  useEffect(() => {
    getTotalNumberOfSearch();
  }, [getTotalNumberOfSearch]);
  return <Statistic name="Number of Searches" value={numOfSearches} />;
}

export default NumberOfSearches;
