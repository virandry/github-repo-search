import React from 'react';
import { Spinner } from '../../../components/Spinner';
import styles from '../Admin.module.scss';
import cx from 'classnames';

function ItemsLoading(props) {
  const { className, ...rest } = props;
  return (
    <div className={cx(styles.itemsLoading, className)} {...rest}>
      <Spinner kind="brand2" />
    </div>
  );
}

export default ItemsLoading;
