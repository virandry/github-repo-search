import React, { useCallback, useEffect, useState } from 'react';
import Top5List from '../../../components/Stats/Top5List';
import ReportService from '../../../services/ReportService';
const top5Init = [];

function Top5Languages() {
  const [top5Languages, setTop5Languages] = useState(top5Init);
  const fetchTop5SearchByLanguage = useCallback(async () => {
    const response = await ReportService.fetchTop5SearchByLanguage();
    setTop5Languages(response);
  }, []);
  useEffect(() => {
    fetchTop5SearchByLanguage();
  }, [fetchTop5SearchByLanguage]);
  return <Top5List data-testid="top5Languages" name="Top 5 Languages" list={top5Languages} />;
}

export default Top5Languages;
