import React, { useCallback, useEffect, useState } from 'react';
import Top5List from '../../../components/Stats/Top5List';
import ReportService from '../../../services/ReportService';
const top5Init = [];

function Top5Topics() {
  const [top5Topics, setTop5Topics] = useState(top5Init);
  const fetchTop5SearchByTopic = useCallback(async () => {
    const response = await ReportService.fetchTop5SearchByTopic();
    setTop5Topics(response);
  }, []);

  useEffect(() => {
    fetchTop5SearchByTopic();
  }, [fetchTop5SearchByTopic]);
  return <Top5List data-testid="top5Topics" name="Top 5 Topics" list={top5Topics} />;
}

export default Top5Topics;
