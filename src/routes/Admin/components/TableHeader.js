import React from 'react';
import { Col } from 'react-flexbox-grid';
import cx from 'classnames';
import styles from '../Admin.module.scss';

function TableHeader(props) {
  const { className, ...rest } = props;
  return (
    <Col xs={12} className={cx(styles.tableHeader, className)} {...rest}>
      <div className={styles.row}>
        <div className={styles.col2}>LOG TIME</div>
        <div className={styles.col2}>TOPIC</div>
        <div className={styles.col2}>LANGUAGE</div>
        <div className={styles.col}>ACTION</div>
        <div className={styles.col}>PAGE</div>
        <div className={styles.col}>TOTAL PAGE</div>
        <div className={styles.col2} />
      </div>
    </Col>
  );
}

export default TableHeader;
