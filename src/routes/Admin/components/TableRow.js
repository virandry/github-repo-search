import React, { useCallback } from 'react';
import { Col } from 'react-flexbox-grid';
import cx from 'classnames';
import styles from '../Admin.module.scss';
import format from 'date-fns/format';
import Label from '../../../components/Label';
import { SearchAction } from '../../../constants/SearchAction';
import Button from '../../../components/Button/Button';

function TableRow(props) {
  const { id, topic, language, page, totalPage, time, url, action, className, result, handleShowSearchResults, ...rest } = props;
  const showResult = useCallback(() => {
    const parsedResult = JSON.parse(result);
    if (handleShowSearchResults) handleShowSearchResults(parsedResult, url);
  }, [handleShowSearchResults, result, url]);
  return (
    <Col xs={12} className={cx(styles.tableContent, className)} {...rest}>
      <div className={styles.row}>
        <div className={styles.col2}>{format(new Date(`${time}`), 'dd MMM yyyy, HH:mm')}</div>
        <div className={styles.col2}>{topic}</div>
        <div className={styles.col2}>{language}</div>
        <div className={styles.col}>
          <Label highlight={action === SearchAction.SUBMIT}>{action}</Label>
        </div>
        <div className={styles.col}>{page}</div>
        <div className={styles.col}>{totalPage}</div>
        <div className={styles.col2}>
          <Button small onClick={showResult}>
            Show Results
          </Button>
        </div>
      </div>
    </Col>
  );
}

export default TableRow;
