import React, { useCallback, useState } from 'react';
import { Col, Row } from 'react-flexbox-grid';
import TableHeader from './TableHeader';
import TableRow from './TableRow';
import ItemsLoading from './ItemsLoading';
import useFetchListWithInfiniteScroll from '../../../hooks/useFetchListWithInfiniteScroll';
import ReportService from '../../../services/ReportService';
import styles from '../Admin.module.scss';
import { Card, CardBasicInfo, CardContent, CardMeta, CardThumbnail } from '../../../components/Card';
import Label from '../../../components/Label';
import { Modal } from '../../../components/Modal';

const modalInit = {
  open: false,
  searchResults: [],
  apiUrl: '',
};

function Audits() {
  const [modal, setModal] = useState(modalInit);
  const handleShowSearchResults = useCallback((searchResults, apiUrl) => {
    setModal({
      open: true,
      searchResults,
      apiUrl,
    });
  }, []);

  const { infiniteRef, list: audits, query, isEmptyList: isAuditsEmpty, loading } = useFetchListWithInfiniteScroll({
    service: ReportService.fetchAll,
  });
  return (
    <>
      <div ref={infiniteRef}>
        <Row data-testid="auditsReports">
          <TableHeader />
          {audits.map(({ id, topic, language, page, totalPage, time, url, action, result }) => {
            return (
              <TableRow
                key={id}
                id={id}
                topic={topic}
                language={language}
                page={page}
                totalPage={totalPage}
                time={time}
                url={url}
                action={action}
                result={result}
                handleShowSearchResults={handleShowSearchResults}
              />
            );
          })}
          {isAuditsEmpty && <span>No records found.</span>}
          {(loading || query.hasNextPage) && <ItemsLoading />}
        </Row>
      </div>
      <Modal open={modal.open} onClose={() => setModal(modalInit)}>
        <Row>
          <Col xs={12} className={styles.modalTitle}>
            Search Result
          </Col>
          <Col xs={12}>
            <div className={styles.apiUrl}>{modal.apiUrl}</div>
          </Col>
          {modal?.searchResults.map(item => {
            return (
              <Col key={item.id} xs={12} sm={6} md={3}>
                <Card id={item.id}>
                  <CardThumbnail src={item.owner?.avatar_url} alt={item.name}>
                    <CardBasicInfo title={item.name} caption={item.language} />
                  </CardThumbnail>
                  <CardContent>
                    <CardMeta stars={item.stargazers_count} forks={item.forks} url={item.html_url} />
                  </CardContent>
                  <CardContent>
                    <div>
                      {item.topics.map((tTopic, index) => (
                        <Label key={index}>{tTopic}</Label>
                      ))}
                    </div>
                  </CardContent>
                </Card>
              </Col>
            );
          })}
        </Row>
      </Modal>
    </>
  );
}

export default Audits;
