import React from 'react';
import styles from './Admin.module.scss';
import { Col, Row } from 'react-flexbox-grid';
import { Logo } from '../../components/Logo';
import { Link } from 'react-router-dom';
import Top5Languages from './components/Top5Languages';
import Top5Topics from './components/Top5Topics';
import NumberOfSearches from './components/NumberOfSearches';
import Audits from './components/Audits';

function Admin() {
  return (
    <div data-testid="admin-page" className={styles.admin}>
      <div className={styles.container}>
        <Logo admin />
        <Row>
          <Col xs={12} sm={4} lg={3} xl={3}>
            <Top5Languages />
          </Col>
          <Col xs={12} sm={4} lg={3} xl={3}>
            <Top5Topics />
          </Col>
          <Col xs={12} sm={4} lg={3} xl={3}>
            <NumberOfSearches />
          </Col>
          <Col xs={12}>
            <Audits />
          </Col>
        </Row>
      </div>
      <div className="navLink">
        <Link to="/">Search Page</Link>&nbsp;|&nbsp;
        <Link to="/login">Logout</Link>
      </div>
    </div>
  );
}

export default Admin;
