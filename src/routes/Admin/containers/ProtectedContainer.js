import React from 'react';
import { Redirect } from 'react-router-dom';
import LocalStorageUtil from '../../../utils/LocalStorageUtil';

function ProtectedContainer(props) {
  const { children } = props;
  if (!LocalStorageUtil.getItem('encodedBasic')) return <Redirect to="/login" />;
  return children;
}

export default ProtectedContainer;
