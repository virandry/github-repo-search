import React from 'react';
import { render, act, cleanup } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { AUDITS_REPORT, NUMBER_OF_SEARCH, TOP_LANGUAGES, TOP_TOPICS } from './fixtures';
import ReportService from '../../../services/ReportService';
import Top5Languages from '../components/Top5Languages';
import Top5Topics from '../components/Top5Topics';
import NumberOfSearches from '../components/NumberOfSearches';
import { waitForElement } from '@testing-library/dom';
import Audits from '../components/Audits';
import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';

jest.mock('../../../services/ReportService');

describe('Admin Page', () => {
  afterEach(cleanup);

  beforeAll(() => {
    ReportService.fetchTop5SearchByLanguage.mockResolvedValue(TOP_LANGUAGES);
    ReportService.fetchTop5SearchByTopic.mockResolvedValue(TOP_TOPICS);
    ReportService.getTotalNumberOfSearch.mockResolvedValue(NUMBER_OF_SEARCH);
    ReportService.fetchAll.mockResolvedValue(AUDITS_REPORT);
    jest.spyOn(React, 'useEffect').mockImplementation(React.useLayoutEffect);
  });
  afterAll(() => React.useEffect.mockRestore());

  it('can load the top 5 languages', async () => {
    const { debug, getByTestId } = render(<Top5Languages />);
    const top5ListNode = await waitForElement(() => getByTestId('top5List'));
    for (const item of TOP_LANGUAGES) {
      expect(top5ListNode).toHaveTextContent(item.name);
      expect(top5ListNode).toHaveTextContent(`${item.count} times`);
    }
    debug();
  });

  it('can load the top 5 topics', async () => {
    const { debug, getByTestId } = render(<Top5Topics />);
    const top5ListNode = await waitForElement(() => getByTestId('top5List'));
    for (const item of TOP_TOPICS) {
      expect(top5ListNode).toHaveTextContent(item.name);
      expect(top5ListNode).toHaveTextContent(`${item.count} times`);
    }
    debug();
  });

  it('can load the number of searches', async () => {
    const { debug, getByTestId } = render(<NumberOfSearches />);
    debug();
    const numberOfSearches = await waitForElement(() => getByTestId('statisticValue'));
    expect(parseInt(numberOfSearches.innerHTML)).toBe(NUMBER_OF_SEARCH);
  });

  it('can load the audits list', async () => {
    const { debug, getByTestId } = render(<Audits />);
    debug();
    const auditsNode = await waitForElement(() => getByTestId('auditsReports'));
    for (const audit of AUDITS_REPORT.content) {
      expect(auditsNode).toHaveTextContent(format(parseISO(audit.time), 'dd MMM yyyy, HH:mm'));
    }
  });
});
