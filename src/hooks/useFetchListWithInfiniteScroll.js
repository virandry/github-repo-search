import { useCallback, useEffect, useState } from 'react';
import useInfiniteScroll from './vendor/useInfiniteScroll';

const iList = [];
const iQuery = {
  page: 0,
  size: 12,
  sort: ['time,desc'],
  hasNextPage: false,
};

export default function useFetchListWithInfiniteScroll(options) {
  const { initList = iList, initQuery = iQuery, service } = options;

  // TODO: There's a possibility that list is undefined and break the apps
  const [list, setList] = useState(initList);
  const [query, setQuery] = useState(initQuery);
  const [loading, setLoading] = useState(false);
  const [isEmptyList, setEmptyList] = useState(false);
  const [totalCurrent, setTotalCurrent] = useState(0);

  useEffect(() => {
    let didCancel = false;
    const loadList = async () => {
      if (!didCancel) setLoading(true);
      try {
        const contentPage = await service({ ...initQuery });
        if (!didCancel) {
          const { content, totalPages, page, totalElements } = contentPage;
          setTotalCurrent(totalElements);
          setList(content);
          const nextPageExists = page + 1 < totalPages;
          setQuery(prevQuery => {
            if (nextPageExists) {
              return { ...prevQuery, page: prevQuery.page + 1, hasNextPage: nextPageExists };
            } else {
              return { ...prevQuery, hasNextPage: nextPageExists };
            }
          });
        }
      } catch (e) {
        console.log(e.message);
      } finally {
        if (!didCancel) setLoading(false);
      }
    };
    loadList();
    return () => {
      didCancel = true;
    };
  }, [initQuery, service]);

  // trigger by useInfiniteScroll whenever query.hasNextPage set to true and it reach certain scroll point;
  const loadMore = useCallback(async () => {
    console.log('load more triggered');
    // list fetching goes here whenever query state is set
    if (!query.hasNextPage) return;
    setLoading(true);
    try {
      const contentPage = await service(query);
      const { content, totalPages, page } = contentPage;
      setList(prevState => [].concat(prevState.concat(content)));
      setQuery(prevQuery => {
        const nextPageExists = page + 1 < totalPages;
        if (nextPageExists) {
          return { ...prevQuery, page: prevQuery.page + 1, hasNextPage: nextPageExists };
        } else {
          return { ...prevQuery, hasNextPage: nextPageExists };
        }
      });
    } catch (e) {
      console.log(e.message);
    } finally {
      setLoading(false);
    }
  }, [query, service]);

  const loadList = useCallback(async () => {
    if (isEmptyList) setEmptyList(false);
    setLoading(true);
    try {
      const contentPage = await service({ ...initQuery });
      const { content, totalPages, page } = contentPage;
      setList(content);
      const nextPageExists = page + 1 < totalPages;
      setQuery(prevQuery => {
        if (nextPageExists) {
          return { ...prevQuery, page: prevQuery.page + 1, hasNextPage: nextPageExists };
        } else {
          return { ...prevQuery, hasNextPage: nextPageExists };
        }
      });
    } catch (e) {
      console.log(e.message);
    } finally {
      setLoading(false);
    }
  }, [initQuery, isEmptyList, service]);

  const infiniteRef = useInfiniteScroll({ hasNextPage: query.hasNextPage, loading, onLoadMore: loadMore, threshold: 600 });

  return {
    infiniteRef,
    list,
    setList,
    totalCurrent,
    loadList,
    isEmptyList,
    setEmptyList,
    loading,
    query,
  };
}
