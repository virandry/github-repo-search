import { useState, useEffect, useCallback } from 'react';

function useWindowSize() {
  const validWindow = typeof window === 'object';

  const getSize = useCallback(() => {
    return {
      width: validWindow ? window.innerWidth : undefined,
      height: validWindow ? window.innerHeight : undefined,
    };
  }, [validWindow]);

  const [size, setSize] = useState(getSize());

  useEffect(() => {
    function handleResize() {
      setSize(getSize());
    }

    if (validWindow) {
      window.addEventListener('resize', handleResize);

      return () => {
        window.removeEventListener('resize', handleResize);
      };
    } else {
      return () => {};
    }
  }, [getSize, validWindow]);

  return size;
}

export default useWindowSize;
