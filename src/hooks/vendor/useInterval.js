import { useRef, useEffect } from 'react';

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }

    if (delay) {
      const id = setInterval(() => {
        tick();
      }, delay);
      return () => clearInterval(id);
    } else {
      return () => {};
    }
  }, [delay]);
}

export default useInterval;
