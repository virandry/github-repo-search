export default class LocalStorageUtil {
  static setItem(name, value) {
    localStorage.setItem(name, JSON.stringify(value));
  }

  static getItem(name) {
    return JSON.parse(localStorage.getItem(name));
  }

  static removeItem(name) {
    window.localStorage.removeItem(name);
  }

  static removeAll() {
    window.localStorage.clear();
  }
}
