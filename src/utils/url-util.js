function camelToSnakeCase(text) {
  return text
    .replace(/(.)([A-Z][a-z]+)/, '$1_$2')
    .replace(/([a-z0-9])([A-Z])/, '$1_$2')
    .toLowerCase();
}

export const queryString = {
  stringify: params => {
    return Object.keys(params)
      .map(key => camelToSnakeCase(key) + '=' + params[key])
      .join('&');
  },
};
