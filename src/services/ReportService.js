import axios from 'axios';
import LocalStorageUtil from '../utils/LocalStorageUtil';
import HttpService from './HttpService';

const adminBaseUrl = 'http://localhost:8080';
const reportBaseUrl = `${adminBaseUrl}/reports`;
const requestConfig = {
  mode: 'no-cors',
  headers: {
    'Content-Type': 'application/json',
  },
};

const http = new HttpService().axios;

function getAuthHeader() {
  const encodedBasicToken = LocalStorageUtil.getItem('encodedBasic');
  if (encodedBasicToken) {
    requestConfig.headers.Authorization = `Basic ${encodedBasicToken}`;
  }
  return requestConfig;
}

const getUrlParams = params => {
  const urlParams = new URLSearchParams();
  if (params.page) {
    urlParams.append('page', params.page);
  }
  if (params.size) {
    urlParams.append('size', params.size);
  }
  if (params.sort) params.sort.forEach(column => urlParams.append('sort', column));
  return urlParams;
};

export default class ReportService {
  static fetchAll(params) {
    const urlParams = getUrlParams(params);
    return http.get(`${reportBaseUrl}/audits?${urlParams}`, getAuthHeader()).then(response => response.data);
  }

  static fetchTop5SearchByLanguage() {
    return http.get(`${reportBaseUrl}/top-languages`, getAuthHeader()).then(response => response.data);
  }

  static fetchTop5SearchByTopic() {
    return http.get(`${reportBaseUrl}/top-topics`, getAuthHeader()).then(response => response.data);
  }

  static getTotalNumberOfSearch() {
    return http.get(`${reportBaseUrl}/searches/total`, getAuthHeader()).then(response => response.data);
  }
}
