import axios from 'axios';

const adminBaseUrl = 'http://localhost:8080';
const requestConfig = {
  mode: 'no-cors',
  headers: {
    'Content-Type': 'application/json',
  },
};

export default class AuditService {
  static log(audit) {
    return axios.post(`${adminBaseUrl}/audits`, audit, requestConfig).then(response => response.data);
  }
}
