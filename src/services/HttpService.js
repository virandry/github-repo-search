import axios from 'axios';

class HttpService {
  axios;

  constructor(baseURL) {
    this.axios = axios.create({
      baseURL,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    this.reponseInterceptor();
  }

  reponseInterceptor() {
    this.axios.interceptors.response.use(
      response => response,
      error => {
        if (error.response.status === 401) window.location = '/login';
        return Promise.reject(error.response || error.message);
      },
    );
  }
}

export default HttpService;
