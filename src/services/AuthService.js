import axios from 'axios';

const adminBaseUrl = 'http://localhost:8080';
const requestConfig = {
  mode: 'no-cors',
  headers: {
    'Content-Type': 'application/json',
  },
};

export default class AuthService {
  static login(username, password) {
    return axios.post(`${adminBaseUrl}/login`, { username, password }, requestConfig).then(response => response.data);
  }
}
