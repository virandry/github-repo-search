import axios from 'axios';
// TODO: old library should be rewritten
import parseLinkHeader from 'parse-link-header';
import { queryString } from '../utils/url-util';

const githubBaseUrl = 'https://api.github.com';
const topicsMediaType = 'application/vnd.github.mercy-preview+json';

const topicsRequestConfig = {
  mode: 'no-cors',
  headers: {
    Accept: topicsMediaType,
    'Content-Type': 'application/json',
  },
};

const username = process.env.USERNAME;
const personalAccessToken = process.env.TOKEN;
console.log(username);
console.log(personalAccessToken);
if (username && personalAccessToken) {
  const token = btoa(`${username}:${personalAccessToken}`);
  topicsRequestConfig.headers.Authorization = `Basic ${token}`;
}

export default class GithubService {
  static searchRepo(query) {
    let q = '';
    if (query.topic) q += `topic:${query.topic}`;
    if (query.language) {
      if (q) q += '+';
      q += `language:${query.language}`;
    }
    const params = {
      q,
      page: query.page,
      perPage: query.perPage,
    };
    const qs = queryString.stringify(params);
    const url = `${githubBaseUrl}/search/repositories?${qs}`;
    return axios.get(url, topicsRequestConfig).then(response => {
      const link = response.headers?.link;
      const pagination = parseLinkHeader(link);
      const lastPage = pagination?.last?.page;
      const parsedLastPage = lastPage ? parseInt(lastPage) : lastPage;
      const prevPage = pagination?.prev?.page;
      const parsedPrevPage = prevPage ? parseInt(prevPage) : prevPage;

      return {
        url,
        totalCount: parseInt(response.data.total_count),
        items: response.data.items,
        currentPage: query.page,
        totalPage: parsedLastPage ?? parsedPrevPage + 1,
      };
    });
  }
}
