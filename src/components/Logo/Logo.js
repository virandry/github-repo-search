import React from 'react';
import styles from './Logo.module.scss';
import cx from 'classnames';

function Logo(props) {
  const { className, admin, ...rest } = props;

  return (
    <div className={cx(styles.logo, className)} {...rest}>
      <div className={styles.textContainer}>
        <span>Github Repo Search Engine</span>
        <div className={styles.decoration} />
        {admin && <div className={styles.admin}>ADMIN</div>}
      </div>
    </div>
  );
}

export default Logo;
