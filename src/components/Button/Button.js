import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import cx from 'classnames';
import style from './Button.module.scss';

function Button(props) {
  const { type = 'button', className, href, children, skew, kind = 'default', fullWidth = false, small, ...rest } = props;
  const btnProps = useMemo(() => {
    return {
      ...rest,
      type,
      className: cx(className, style.btn, style[`${kind}Kind`], {
        [style.skew]: skew,
        [style.fullWidth]: fullWidth,
        [style.small]: small,
      }),
    };
  }, [small, fullWidth, kind, skew, className, rest, type]);
  if (href)
    return (
      <a href={href} {...btnProps}>
        {children}
      </a>
    );
  return <button {...btnProps}>{children}</button>;
}

export default Button;

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  fullWidth: PropTypes.bool,
  href: PropTypes.string,
  kind: PropTypes.string,
  skew: PropTypes.bool,
  small: PropTypes.bool,
  type: PropTypes.string,
};
