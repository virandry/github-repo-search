import React from 'react';
import cx from 'classnames';
import styles from './Card.module.scss';
import githubIcon from '../../assets/github.png';

function CardMeta(props) {
  const { stars, forks, url, className, ...rest } = props;
  return (
    <div className={cx(styles.meta, className)} {...rest}>
      <div className={cx(styles.stars)}>{`${stars} stars`}</div>
      &nbsp;•&nbsp;
      <div className={cx(styles.forks)}>{`${forks} forks`}</div>
      &nbsp;•&nbsp;
      <div className={cx(styles.github)}>
        <a href={url} target="_blank" rel="noopener noreferrer">
          <img className={styles.icon} src={githubIcon} />
        </a>
      </div>
    </div>
  );
}

export default CardMeta;
