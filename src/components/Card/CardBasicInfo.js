import React from 'react';
import cx from 'classnames';
import styles from './Card.module.scss';

function CardBasicInfo(props) {
  const { title, caption, className, ...rest } = props;
  return (
    <div className={cx(styles.infoWrapper, className)} {...rest}>
      <div className={cx(styles.title)}>{title}</div>
      {caption && <div className={cx(styles.caption)}>{caption}</div>}
    </div>
  );
}

export default CardBasicInfo;
