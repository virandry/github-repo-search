import React from 'react';
import cx from 'classnames';
import styles from './Card.module.scss';

function CardThumbnail(props) {
  const { src, alt, children, className, ...rest } = props;
  return (
    <div className={cx(styles.thumbnail, className)} {...rest}>
      <img className={cx(styles.image)} src={src} alt={alt} />
      <div className={cx(styles.mask)} />
      {children}
    </div>
  );
}

export default CardThumbnail;
