import React from 'react';
import { render } from '@testing-library/react';
import { Card, CardBasicInfo, CardContent, CardMeta, CardThumbnail } from '../index';
import Label from '../../Label';
import { CARDS_DATA } from './fixtures';

function getCard(item) {
  return (
    <Card id={item.id}>
      <CardThumbnail src={item.owner?.avatar_url} alt={item.name}>
        <CardBasicInfo title={item.name} caption={item.language} />
      </CardThumbnail>
      <CardContent>
        <CardMeta stars={item.stargazers_count} forks={item.forks} url={item.html_url} />
      </CardContent>
      <CardContent>
        <div>
          {item.topics.map((tTopic, index) => (
            <Label key={index}>{tTopic}</Label>
          ))}
        </div>
      </CardContent>
    </Card>
  );
}

describe('Card Component', () => {
  it('can render with complete props', () => {
    const item = CARDS_DATA[0];
    const { debug, rerender, getByText, queryByText } = render(getCard(item));
    debug();
    expect(getByText(item.name)).toBeInTheDocument();

    const item2 = CARDS_DATA[1];
    rerender(getCard(item2));
    expect(queryByText(item.name)).not.toBeInTheDocument();
    expect(getByText(item2.name)).toBeInTheDocument();
  });
});
