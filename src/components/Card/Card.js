import React from 'react';
import cx from 'classnames';
import styles from './Card.module.scss';

export default function Card(props) {
  const { className, children, ...rest } = props;

  return (
    <div className={cx(styles.card, className)} {...rest}>
      {children}
    </div>
  );
}
