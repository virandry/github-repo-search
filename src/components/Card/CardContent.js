import React from 'react';
import cx from 'classnames';
import styles from './Card.module.scss';

function CardContent(props) {
  const { children, className, ...rest } = props;
  return (
    <div className={cx(styles.content, className)} {...rest}>
      {children}
    </div>
  );
}

export default CardContent;
