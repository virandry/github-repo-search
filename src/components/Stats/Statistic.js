import React, { useMemo } from 'react';
import styles from './Statistic.module.scss';

function Statistic(props) {
  const { name, value, format, className, ...rest } = props;
  const formattedValue = useMemo(() => {
    if (format === 'percentage') return `${value * 100}%`;
    return value;
  }, [format, value]);
  return (
    <div className={styles.statistic} {...rest}>
      <div data-testid="statisticValue" className={styles.value}>
        {formattedValue}
      </div>
      <div className={styles.name}>{name}</div>
    </div>
  );
}

export default Statistic;
