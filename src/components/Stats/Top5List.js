import React from 'react';
import styles from './Top5List.module.scss';
import cx from 'classnames';

function Top5List(props) {
  const { name, list, className, type, ...rest } = props;

  return (
    <div className={cx(styles.top5List, styles[`${type}Type`], className)} {...rest}>
      <div className={styles.name}>{name}</div>
      {list?.length ? (
        <ul data-testid="top5List" className={styles.items}>
          {list.map((item, index) => {
            return <li key={index} className={styles.item}>{`${item.name} (${item.count} times)`}</li>;
          })}
        </ul>
      ) : (
        <div className={styles.noData}>
          <div className={styles.title}>Collecting Data</div>
          <div className={styles.caption}>Please use the search more to collect statistic data.</div>
        </div>
      )}
    </div>
  );
}

export default Top5List;
