import React from 'react';
import styles from './Spinner.module.scss';
import cx from 'classnames';

function Spinner(props) {
  const { className, kind, ...rest } = props;
  return (
    <div className={cx(styles.loadingSpinner, className, styles[`${kind}Kind`])} {...rest}>
      <div />
    </div>
  );
}

export default Spinner;
