import React from 'react';
import ReactResponsiveModal from 'react-responsive-modal';
import cx from 'classnames';
import styles from './Modal.module.scss';

function Modal(props) {
  const { children, className, open, onClose, ...rest } = props;
  return (
    <ReactResponsiveModal
      open={open}
      onClose={onClose}
      showCloseIcon={false}
      center
      classNames={{
        overlay: styles.customOverlay,
        modal: cx(styles.customModal, className),
      }}
      {...rest}
    >
      {children}
    </ReactResponsiveModal>
  );
}

export default Modal;
