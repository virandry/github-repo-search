import React, { useMemo } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import styles from './Input.module.scss';

export default function Input(props) {
  const { id, label, size, innerRef, className, wrapperClassName, ...rest } = props;

  const input = useMemo(() => {
    const inputProps = {
      ...{
        ...rest,
        id: id,
        ref: innerRef,
        className: cx(className, styles.input, {
          [styles.inputSm]: ['sm', 'small'].indexOf(size) !== -1,
          [styles.inputLg]: ['lg', 'large'].indexOf(size) !== -1,
        }),
      },
    };
    return <input {...inputProps} />;
  }, [id, className, innerRef, rest, size]);

  return (
    <div className={cx(wrapperClassName, styles.inputContainer)}>
      {label && (
        <label htmlFor={id} className={styles.label}>
          {label}
        </label>
      )}
      {input}
    </div>
  );
}

Input.propTypes = {
  className: PropTypes.string,
  innerRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  size: PropTypes.string,
};
