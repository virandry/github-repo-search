import React, { useMemo } from 'react';
import cx from 'classnames';
import styles from './Label.module.scss';

function Label(props) {
  const { children, className, highlight, ...rest } = props;
  const content = useMemo(() => {
    return typeof children === 'string' ? <span>{children}</span> : children;
  }, [children]);
  return (
    <div className={cx(styles.label, className, { [styles.highlight]: highlight })} {...rest}>
      {content}
    </div>
  );
}

export default Label;
