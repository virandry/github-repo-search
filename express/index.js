const express = require('express');
const path = require('path');
const expressStaticGzip = require('express-static-gzip');

const port = process.env.PORT || 3000;
const app = express();

// serve static assets normally
app.use(
  '/',
  expressStaticGzip(`${__dirname}/../dist`, {
    enableBrotli: true,
    index: false,
    orderPreference: ['br', 'gzip'],
  }),
);

app.get('*', (request, response) => {
  response.sendFile(path.resolve(__dirname, '../dist', 'index.html'));
});

app.listen(port);
console.log(`server started on port ${port}`);
